import unittest
from typing import List


def to_int_list(value):
    if isinstance(value, list):
        return value
    return [int(i) for i in value.split()]


def quick_merge(data):
    if len(data) == 1:
        return to_int_list(data[0])
    elif len(data) == 2:
        result = []
        i = 0
        j = 0
        list1 = to_int_list(data[0])
        list2 = to_int_list(data[1])
        while i < len(list1) and j < len(list2):
            if list1[i] > list2[j]:
                result.append(list2[j])
                j += 1
            elif list1[i] == list2[j]:
                result.append(list2[j])
                i += 1
                j += 1
            else:
                result.append(list1[i])
                i += 1

        if i < len(list1):
            result += list1[i:]
        if j < len(list2):
            result += list2[j:]

        return result
    else:
        result = quick_merge(data[0:2])

        for i in range(2, len(data)):
            result = quick_merge([result, data[i]])

        return result


class Test(unittest.TestCase):
    def test_quick_merge(self):
        self.assertEqual(quick_merge(['1 2 3 4']), [1, 2, 3, 4])

        self.assertEqual(quick_merge(['1 2 3 4', '2 5 6']), [1, 2, 3, 4, 5, 6])

        self.assertEqual(
            quick_merge(["3", "1 2 3 4", "5 6 7", "10 11 17"]),
            [1, 2, 3, 4, 5, 6, 7, 10, 11, 17])


if __name__ == '__main__':
    unittest.main()
