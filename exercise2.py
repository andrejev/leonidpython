
value = int(input())

for i in range(1, value + 1):
    for x in range(1, i + 1):
        print(x, end='')
    for x in range(i - 1, 0, -1):
        print(x, end='')
    print()
