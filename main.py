import unittest


def exercise1(dna: str):
    result = ""
    count = 0
    last_character = dna[0]
    for ch in dna:
        if ch == last_character:
            count += 1
        else:
            result += last_character + str(count)
            count = 1
            last_character = ch

    result += last_character + str(count)

    return result


class Test(unittest.TestCase):
    def test_exercise1(self):
        self.assertEqual(exercise1('aaaabbcaa'), 'a4b2c1a2')
        self.assertEqual(exercise1('abc'), 'a1b1c1')


if __name__ == '__main__':
    unittest.main()
