import unittest


def is_palindrome(text):
    i_begin = 0
    i_end = len(text) - 1

    while i_begin <= i_end:
        if not text[i_begin].isalpha():
            i_begin += 1
            continue

        if not text[i_end].isalpha():
            i_end -= 1
            continue

        if text[i_begin] != text[i_end]:
            return False

        i_begin += 1
        i_end -= 1

    return True


class Test(unittest.TestCase):
    def test_is_palindrome(self):
        self.assertTrue(is_palindrome('aba'))
        self.assertTrue(is_palindrome('abb,a   '))
        self.assertFalse(is_palindrome('abaacba'))

        self.assertTrue(is_palindrome('abacaba'))


if __name__ == '__main__':
    unittest.main()
